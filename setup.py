# setup.py
import setuptools


# load README.md
with open("README.md", "r") as f:
    load_description = f.read()


setuptools.setup(
    name="Request-Kintone",
    version="0.0.3",
    author="tkok",
    author_email="okawabb@gmail.com",
    description="Useful module for connecting Kintone webapi",
    long_description=load_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/tkok/request-kintone",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    install_requires=[
        'requests',
    ]
)
