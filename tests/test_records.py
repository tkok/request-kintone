"""
Unit test for record.py
"""
import os
import unittest
from datetime import datetime
from unittest.mock import Mock, patch, mock_open

from reqkintone.records import Records, Query
from reqkintone.base import RequestApi, Model

from requests.exceptions import Timeout, ConnectionError


class TestRecords(unittest.TestCase):
    def test_init_should_raise_error_if_model_is_unexpected(self):

        with self.assertRaisesRegex(TypeError, "model is unexpected"):
            m = 123
            Records(1, m)

    def test_init_should_assign_props_properly(self):
        m = Model("name")
        r = Records(1, m)
        self.assertEqual(1, r._Records__app_id)
        self.assertEqual(m, r._Records__model)
        self.assertTrue(r._Records__total_count)

    def test_set_total_count_should_set_private_property(self):
        m = Model("name")
        r = Records(1, m)

        self.assertTrue(r._Records__total_count)
        r.set_total_count(False)
        self.assertFalse(r._Records__total_count)

    def test_set_query_should_raise_error_if_query_is_unexpected(self):
        m = Model("name")
        r = Records(1, m)
        with self.assertRaisesRegex(TypeError, "query is unexpected value"):
            r.set_query(123)

    def test_set_query_should_assign_private_props(self):
        m = Model("name")
        r = Records(1, m)
        q = Query()
        self.assertFalse(r._Records__query)
        r.set_query(q)
        self.assertEqual(type(r._Records__query), Query)

    def test_set_values_should_return_list_assigned_expected_values_as_model(self):
        m = Model("name")
        m.add_field("user", m.ATTRIBUTE_TYPE["STRING"])
        m.add_field("pid", m.ATTRIBUTE_TYPE["INTEGER"])
        r = Records(1, m)
        res = {
            "name": [
                {"user": {"value": "aa"}, "pid": {"value": 1}},
                {"user": {"value": "bb"}, "pid": {"value": 2}},
            ]
        }
        models = r._set_values(res["name"])
        self.assertEqual(2, len(models))
        self.assertNotEqual(id(models[0]), id(models[1]))
        self.assertEqual(
            models[0].fields["user"]["value"], res["name"][0]["user"]["value"]
        )

    @patch("reqkintone.base.requests")
    def test_get_should_return_error_if_request_is_failed(self, mock_req):
        message = "ConnectionError occured."
        mock_req.get.side_effect = ConnectionError(message)

        m = Model("name")
        r = Records(1, m)
        r.url = "123"
        r.authorize_code = "123"

        self.assertFalse(r.get()[0])
        self.assertEqual(r.get()[1], mock_req.get.side_effect)

    @patch("reqkintone.base.requests")
    def test_get_should_return_true_if_can_not_assign_model(self, mock_req):

        json = {
            "records": [
                {
                    "date_a": {"value": "2019-01-01"},
                    "user": {"value": "pasio"},
                    "pid": {"value": 123},
                }
            ]
        }

        class response:
            status_code = 200

            def json(self):
                return json

        mock_req.get.return_value = response()

        m = Model("records")
        r = Records(1, m)
        r.url = "123"
        r.authorize_code = "123"
        res = r.get()
        self.assertEqual(True, res[0])
        self.assertFalse(res[1][0].fields)

    @patch("reqkintone.base.requests")
    def test_get_should_return_expected_model_if_success_to_assign(self, mock_req):
        json = {
            "records": [
                {
                    "date_a": {"value": "2019-01-01T11:11:11Z"},
                    "user": {"value": "pasio"},
                    "pid": {"value": 123},
                },
                {
                    "date_a": {"value": "2019-02-02T22:22:22Z"},
                    "user": {"value": "baby"},
                    "pid": {"value": 456},
                },
            ]
        }

        class response:
            status_code = 200

            def json(self):
                return json

        mock_req.get.return_value = response()

        m = Model("records")
        m.add_field("date_a", m.ATTRIBUTE_TYPE["DATETIME"])
        m.add_field("user", m.ATTRIBUTE_TYPE["STRING"])
        m.add_field("pid", m.ATTRIBUTE_TYPE["INTEGER"])
        r = Records(1, m)
        r.url = "123"
        r.authorize_code = "123"
        res = r.get()
        self.assertTrue(res[0])
        self.assertEqual(
            json["records"][0]["user"]["value"], res[1][0].fields["user"]["value"]
        )
        self.assertEqual(
            json["records"][1]["pid"]["value"], res[1][1].fields["pid"]["value"]
            )

