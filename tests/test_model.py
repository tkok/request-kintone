"""
Unit test for base.Model
"""
import os
import unittest
from datetime import datetime
from unittest.mock import Mock, patch, mock_open

from reqkintone.base import Model


class TestModel(unittest.TestCase):
    def test_init(self):
        m = Model("record")
        self.assertEqual("record", m.name)
        self.assertEqual({}, m.fields)

    def test_add_field_should_return_assigned_field(self):
        m = Model("record")
        res = m.add_field("hello", m.ATTRIBUTE_TYPE["STRING"])
        expected = {"type": m.ATTRIBUTE_TYPE["STRING"], "value": None}
        self.assertEqual(expected, res)

    def test_add_field_should_return_false_if_field_is_existed(self):
        m = Model("record")
        m.add_field("hello", m.ATTRIBUTE_TYPE["STRING"])
        res = m.add_field("hello", m.ATTRIBUTE_TYPE["STRING"])
        self.assertFalse(res)

    def test_set_value_return_false_if_field_is_not_existed(self):
        m = Model("record")
        res = m.set_value("hello", "world")
        self.assertFalse(res)

    def test_set_value_return_int_value_if_field_is_int(self):
        m = Model("record")
        m.add_field("hello", m.ATTRIBUTE_TYPE["INTEGER"])
        res = m.set_value("hello", 123)
        self.assertEqual(123, res)
        self.assertEqual(type(res), int)

    def test_set_value_should_return_if_type_is_different(self):
        m = Model("record")
        # type integer, value string -> return False
        m.add_field("hello1", m.ATTRIBUTE_TYPE["INTEGER"])
        res = m.set_value("hello1", "aaa")
        self.assertFalse(res)
        # type string, value integer -> return string
        m.add_field("hello2", m.ATTRIBUTE_TYPE["STRING"])
        res = m.set_value("hello2", 123)
        self.assertEqual(res, "123")

        # type datetime, value integer
        m.add_field("hello3", m.ATTRIBUTE_TYPE["DATETIME"])
        res = m.set_value("hello3", 123)
        self.assertFalse(res)

        # value different format
        res = m.set_value("hello3", "2019-01-01")
        self.assertFalse(res)
        res = m.set_value("hello3", "2019-01-01 01:01:01")
        self.assertFalse(res)
        # invalid datetime
        res = m.set_value("hello3", "2019-01-40T01:01:01Z")
        self.assertFalse(res)
        res = m.set_value("hello3", "2019-01-01T01:01:01Z")
        self.assertEqual(type(res), datetime)

    def test_set_value_should_return_updated_value_if_new_value_is_assigned(self):
        m = Model("record")
        m.add_field("hello", m.ATTRIBUTE_TYPE["INTEGER"])
        res = m.set_value("hello", 123)
        self.assertEqual(123, res)
        res = m.set_value("hello", 999)
        self.assertEqual(999, res)
        res = m.set_value("hello", "world")
        self.assertFalse(res)
        self.assertEqual(999, m.fields["hello"]["value"])
