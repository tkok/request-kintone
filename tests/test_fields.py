"""
Unit test for fields.py
"""
import os
import unittest
from datetime import datetime
from unittest.mock import Mock, patch, mock_open

from reqkintone.fields import Fields
from reqkintone.base import RequestApi

from requests.exceptions import Timeout, ConnectionError


class TestFields(unittest.TestCase):
    def setUp(self):
        self.fields = Fields(11)
    def tearDown(self):
        del self.fields

    def test_init_should_assign_expected_props(self):
        f = self.fields
        self.assertEqual(11, f._Fields__app_id)
    
    def test_add_fields_should_reject_arg_except_string(self):
        f = self.fields
        cur_fields = []
        self.assertEqual(cur_fields, f._Fields__names)
        new_fields = f.add_name(99)
        self.assertEqual(cur_fields, new_fields)
        new_fields = f.add_name([123])
        self.assertEqual(cur_fields, new_fields)
        new_fields = f.add_name("hello")
        self.assertNotEqual(cur_fields, new_fields)
    
    def test_format_response_should_return_expected_list(self):
        test = {
            "aaa": [1,2,3],
            "bbb": [4,5,6],
            "ccc": [7,8,9]
        }
        f = self.fields
        self.assertEqual(test, f._format_response(test))
        f.add_name("ddd")
        self.assertEqual({}, f._format_response(test))
        f.add_name("bbb")
        self.assertEqual({"bbb":[4,5,6]}, f._format_response(test))
        f.add_name("aa") # not match
        f.add_name("ccc")
        self.assertEqual(
            {"bbb":[4,5,6],"ccc":[7,8,9]}, 
            f._format_response(test)
            )

    @patch.object(RequestApi, "_get", return_value=[400, "error"])
    def test_get_should_return_failed_response_if_response_is_400(self, get):
        """if response status code is not 200"""
        f = self.fields
        self.assertEqual((False, "error"), f.get())
    
    @patch.object(RequestApi, "_get", return_value=[200, {"error":"wrong"}])
    def test_get_should_return_failed_response_if_unexpected_response(self, get):
        """if response status code is 200 but response not include properties"""
        f = self.fields
        self.assertEqual((False, {"error":"wrong"}), f.get())

    @patch.object(RequestApi, "_get",
        return_value=[
            200, {"properties":{"aaa":1,"bbb":"hello","ccc":"world"}}
        ]
    )
    def test_get_should_return_successful_response(self, get):
        """if response status code is 200 and response includes users"""
        f = self.fields
        self.assertEqual((True, get.return_value[1]["properties"]), f.get())
        f.add_name("bbb")
        self.assertEqual((True, {"bbb":"hello"}), f.get())
