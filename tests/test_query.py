"""
Unit test for Query in records.py
"""
import os
import unittest
from datetime import datetime
from unittest.mock import Mock, patch, mock_open
import json

from requests.exceptions import Timeout, ConnectionError

from reqkintone.records import Query
from reqkintone.base import RequestApi, Model


class TestQuery(unittest.TestCase):
    def setUp(self):
        self.q = Query()

    def test_init(self):
        q = self.q
        self.assertEqual([], q._Query__queries)
        self.assertEqual([], q._Query__orders)
        self.assertEqual(-1, q._Query__limit)
        self.assertEqual(-1, q._Query__offset)

    def test_set_limitation_should_assign_limit_and_offset_same_time(self):
        q = self.q

        q.set_limitation(5, 10)
        self.assertEqual(10, q._Query__limit)
        self.assertEqual(5, q._Query__offset)

        self.q = Query()
        q = self.q

        q.set_limitation(20)
        self.assertEqual(-1, q._Query__limit)
        self.assertEqual(20, q._Query__offset)

    def test_set_order_should_assign_expected_value_to_property(self):
        q = self.q
        exp = []
        exp.append({"key": "平均", "asc": True})
        q.set_order("平均")
        self.assertEqual(exp, q._Query__orders)

        exp.append({"key": "average", "asc": False})
        q.set_order("average", False)
        self.assertEqual(exp, q._Query__orders)

    def test_make_option_query_should_return_empty_string_if_not_asigned_option_params(
        self
    ):
        q = self.q
        res = q._make_option_query()
        self.assertFalse(res)

    def test_make_option_query_should_return_expected_query_if_assigned_option_params(
        self
    ):
        q = self.q
        q.set_order("平均")
        res = q._make_option_query()
        exp = "order by 平均 asc"
        self.assertEqual(res, exp)

        q.set_order("average", False)
        q.set_order("max", True)
        res = q._make_option_query()
        exp = "order by 平均 asc, average desc, max asc"
        self.assertEqual(res, exp)

        self.q = Query()
        q = self.q
        q.set_limitation(10)
        res = q._make_option_query()
        self.assertEqual(res, "offset 10")
        q.set_limitation(30, 20)
        res = q._make_option_query()
        self.assertEqual(res, "limit 20 offset 30")
        q.set_order("平均")
        q.set_order("average", False)
        res = q._make_option_query()
        self.assertEqual(res, "order by 平均 asc, average desc limit 20 offset 30")

    def test_make_condition_should_return_expected_str_in_case_value_is_int(self):
        q = self.q
        cond = q.make_condition("aa", 123, "<=")
        self.assertEqual("aa <= 123", cond)

    def test_make_condition_should_return_expected_str_in_case_value_is_str(self):
        q = self.q
        cond = q.make_condition("更新日時", "2019-01-01T09:00:00+0900", ">")
        self.assertEqual('更新日時 > "2019-01-01T09:00:00+0900"', cond)

        cond = q.make_condition("企業マスタ.企業名", ("パシオ", "サムライ"), "in")
        self.assertEqual("企業マスタ.企業名 in ('パシオ', 'サムライ')", cond)

    def test_add_should_return_expected_queries(self):
        q = self.q
        q.add("abc", "hello", "!=")
        queries = q.add("def", 123, ">=", False)
        self.assertEqual(2, len(queries))
        self.assertFalse(queries[1]["and_logical"])

    def test_query_string_should_return_empty_if_not_assigned_params(self):
        q = self.q
        string = q.query_string()
        self.assertFalse(string)

    def test_query_string_should_return_expected_string(self):
        q = self.q
        q.set_limitation(10, 20)
        q.set_order("更新日時", False)
        q.add("名前", "パシオ", "!=")
        q.add("average", 5, ">=")

        s = (
            q.make_condition("user", (1, 2), "not in")
            + " or "
            + q.make_condition("name", ("hello", "world"), "in")
        )
        q.add_by_str(s, and_logical=False)

        string = q.query_string()

        exp = '名前 != "パシオ" and average >= 5 or '
        exp += "(user not in (1, 2) or name in ('hello', 'world'))"
        exp += " order by 更新日時 desc limit 20 offset 10"
        self.assertEqual(exp, string)

    def test_query_string_should_return_expected_string_in_case_only_option(self):
        q = self.q
        q.set_limitation(10, 20)
        q.set_order("更新日時", False)
        exp = "order by 更新日時 desc limit 20 offset 10"
        string = q.query_string()
        self.assertEqual(exp, string)
