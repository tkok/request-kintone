"""
Unit test for users.py
"""
import unittest
from datetime import datetime
from unittest.mock import patch

from reqkintone.users import Users
from reqkintone.base import RequestApi

from requests.exceptions import Timeout, ConnectionError


class TestUsers(unittest.TestCase):
    
    def test_init_should_extends_request_api(self):
        u = Users()
        self.assertTrue("authorize_code" in dir(u))
        self.assertTrue("_set_headers" in dir(u))

    def test_init_should_assign_props(self):
        u = Users()
        self.assertEqual(u._Users__fields, u.DEFAULT_RETURN_FIELDS)

    def test_add_code_append_code_and_should_return_updated_codes(self):
        u = Users()
        new = u.add_code("hello")
        self.assertEqual(["hello"], new)
        new = u.add_code("world")
        self.assertEqual(["hello", "world"], new)

    def test_add_code_should_not_append_code_if_not_str_is_passed(self):
        u = Users()
        new = u.add_code(1)
        self.assertEqual(new, [])
        new = u.add_code([1, 2, 3])
        self.assertEqual(new, [])
        new = u.add_code("ok")
        self.assertEqual(new, ["ok"])

    def test_add_id_append_id_and_should_return_updated_ids(self):
        u = Users()
        new = u.add_id("hello")
        self.assertEqual([], new)
        new = u.add_id(123)
        self.assertEqual([123], new)
        new = u.add_id(456)
        self.assertEqual([123,456], new)

    def test_set_fields_should_assign_expected_values(self):
        u = Users()
        u.set_fields()
        self.assertEqual(u.DEFAULT_RETURN_FIELDS, u._Users__fields)
        u.set_fields(123)
        self.assertEqual(u.DEFAULT_RETURN_FIELDS, u._Users__fields)
        u.set_fields(["abc", "def"])
        self.assertEqual(["abc", "def"], u._Users__fields)
    
    def test_query_params_as_dict_should_return_empty(self):
        """if neither codes nor ids are empty"""
        u = Users()
        self.assertFalse(u._query_params_as_dict())
    
    def test_query_params_as_dict_should_return_expected_dict(self):
        """if codes are assigned"""
        u = Users()
        u.add_code("abc")
        expected = {"codes[0]":"abc","size":1}
        self.assertEqual(expected, u._query_params_as_dict())
        del u
        u = Users()
        u.add_code("abc")
        u.add_code("def")
        expected = {"codes[0]":"abc","size":2,"codes[1]":"def"}
        self.assertEqual(expected, u._query_params_as_dict())
        del u
        u = Users()
        u.add_code("abc")
        u.add_code("def")
        u.add_id(123)
        expected = {"codes[0]":"abc","size":2,"codes[1]":"def"}
        self.assertEqual(expected, u._query_params_as_dict())
        del u
    
    def test_query_params_as_dict_should_return_expected_dict_for_ids(self):
        """if ids are assigned"""
        u = Users()
        u.add_id(123)
        u.add_id(456)
        expected = {"ids[0]":123,"size":2,"ids[1]":456}
        self.assertEqual(expected, u._query_params_as_dict())
    
    def test_format_response_should_return_empty(self):
        """if response not included expected fields"""
        response = {"users": [{"birth":1,"place":2},{"birth":3,"place":4}]}
        u = Users()
        self.assertEqual([], u._format_response(response))

    def test_format_response_should_return_expected_list(self):
        """if response includes expected fields"""
        response = {"users": [{"birth":1,"place":2},{"birth":3,"place":4}]}
        u = Users()
        u.set_fields(["birth", "place"])
        self.assertEqual(response["users"], u._format_response(response))

    @patch.object(RequestApi, "_get", return_value=[400, "error"])
    def test_get_should_return_failed_response_if_response_is_400(self, get):
        """if response status code is not 200"""
        u = Users()
        self.assertEqual((False, "error"), u.get())

    @patch.object(RequestApi, "_get", return_value=[200, {"error":"wrong"}])
    def test_get_should_return_failed_response_if_unexpected_response(self, get):
        """if response status code is 200 but response not include users"""
        u = Users()
        self.assertEqual((False, {"error":"wrong"}), u.get())

    @patch.object(RequestApi, "_get",
        return_value=[200, {"users":[{"id":1,"name":"hello","code":"world"}]}])
    def test_get_should_return_successful_response(self, get):
        """if response status code is 200 and response includes users"""
        u = Users()
        self.assertEqual((True, get.return_value[1]["users"]), u.get())

