"""
Unit test for base.RequestApi
"""
import json
import unittest
from unittest.mock import Mock, patch
from requests.exceptions import Timeout, ConnectionError

from reqkintone.base import RequestApi
from reqkintone.config import KINTONE_CONFIG


class TestRequestApi(unittest.TestCase):
    def setUp(self):
        self.req = RequestApi()

    def test_init(self):
        r = self.req
        self.assertRegex(str(r), "RequestApi")

    def test_set_headers(self):
        r = self.req
        r.set_request_content_type_as_json()
        res = r._set_headers()
        exp = {"Content-type": "application/json"}
        self.assertEqual(exp, res)

        r.authorize_code = "hello"
        res = r._set_headers()
        exp["X-Cybozu-Authorization"] = "hello"
        self.assertEqual(exp, res)

    def test_get_should_return_false_if_url_is_empty(self):
        r = self.req
        self.assertFalse(r._get()[0])

    @patch("reqkintone.base.requests")
    def test_get_should_return_false_if_request_is_timeout(self, mock_req):
        message = "Timeout occured."
        mock_req.get.side_effect = Timeout(message)
        r = self.req
        r.url = "http://aa.com"
        res = r._get()
        self.assertFalse(res[0])
        self.assertEqual(message, str(r.request_error))

    @patch("reqkintone.base.requests")
    def test_get_should_return_false_if_request_is_connection_error(self, mock_req):
        message = "ConnectionError occured."
        mock_req.get.side_effect = ConnectionError(message)
        r = self.req
        r.url = "http://aa.com"
        res = r._get()
        self.assertFalse(res[0])
        self.assertEqual(message, str(r.request_error))

    @patch("reqkintone.base.requests")
    def test_get_should_return_list_expected_if_request_is_success(self, mock_req):
        expected_json = {"a": 1, "b": 2}
        expected_url = "http://aa.com"
        expected_param = {"c": 3, "d": 4}

        class response:
            status_code = 200

            def json(self):
                return expected_json

        mock_req.get.return_value = response()
        r = self.req
        r.url = expected_url
        r.set_request_content_type_as_json()
        res = r._get(expected_param)
        self.assertEqual(2, len(res))
        self.assertEqual(200, res[0])
        self.assertEqual(expected_json, res[1])
        mock_req.get.assert_called_with(
            expected_url,
            headers={"Content-type": KINTONE_CONFIG["REQUEST_CONTENT_TYPE_JSON"]},
            json=expected_param,
            timeout=KINTONE_CONFIG["REQUEST_TIMEOUT"],
        )

    def test_put_should_return_false_if_not_having_required_props(self):
        r = self.req
        res = r._put()
        self.assertFalse(res[0])
        r.url = "http://aa.com"
        res = r._put()
        self.assertFalse(res[0])
