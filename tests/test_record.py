"""
Unit test for record.py
"""
import os
import unittest
from datetime import datetime
from unittest.mock import Mock, patch, mock_open

from reqkintone.record import Record
from reqkintone.base import RequestApi, Model

from requests.exceptions import Timeout, ConnectionError


class TestRecord(unittest.TestCase):
    def test_init_should_raise_error_if_model_is_not_unexpected(self):

        with self.assertRaises(TypeError):
            m = 123
            Record(1, 1, m)

    def test_init_should_assign_props_properly(self):
        m = Model("name")
        r = Record(2, 1, m)

        self.assertEqual(1, r._Record__record_id)
        self.assertEqual(2, r._Record__app_id)
        self.assertEqual(m, r._Record__model)

    @patch("reqkintone.base.requests")
    def test_get_should_return_error_if_request_is_failed(self, mock_req):
        message = "ConnectionError occured."
        mock_req.get.side_effect = ConnectionError(message)

        m = Model("name")
        r = Record(2, 1, m)
        r.url = "123"
        r.authorize_code = "123"

        self.assertFalse(r.get()[0])

    @patch("reqkintone.base.requests")
    def test_get_should_return_true_if_can_not_assign_model(self, mock_req):

        json = {
            "record": {
                "date_a": {"value": "2019-01-01"},
                "user": {"value": "pasio"},
                "pid": {"value": 123},
            }
        }

        class response:
            status_code = 200

            def json(self):
                return json

        mock_req.get.return_value = response()

        m = Model("record")
        r = Record(2, 1, m)
        r.url = "123"
        r.authorize_code = "123"
        res = r.get()
        self.assertEqual(True, res[0])
        self.assertFalse(m.fields)

    @patch("reqkintone.base.requests")
    def test_get_should_return_expected_model_if_success_to_assign(self, mock_req):
        json = {
            "record": {
                "date_a": {"value": "2019-01-01T11:11:11Z"},
                "user": {"value": "pasio"},
                "pid": {"value": 123},
            }
        }

        class response:
            status_code = 200

            def json(self):
                return json

        mock_req.get.return_value = response()

        m = Model("record")
        m.add_field("date_a", m.ATTRIBUTE_TYPE["DATETIME"])
        m.add_field("user", m.ATTRIBUTE_TYPE["STRING"])
        m.add_field("pid", m.ATTRIBUTE_TYPE["INTEGER"])
        r = Record(2, 1, m)
        r.url = "123"
        r.authorize_code = "123"
        res = r.get()
        self.assertTrue(res[0])
        self.assertEqual(
            json["record"]["user"]["value"], res[1].fields["user"]["value"]
        )
        self.assertEqual(json["record"]["pid"]["value"], res[1].fields["pid"]["value"])

    @patch("reqkintone.base.requests")
    def test_update_should_return_false_if_request_is_failed(self, mock_req):
        message = "ConnectionError occured."
        mock_req.put.side_effect = ConnectionError(message)

        m = Model("name")
        m.add_field("date_a", m.ATTRIBUTE_TYPE["DATETIME"])
        m.add_field("user", m.ATTRIBUTE_TYPE["STRING"])
        m.add_field("pid", m.ATTRIBUTE_TYPE["INTEGER"])
        r = Record(2, 1, m)
        r.url = "123"
        r.authorize_code = "123"
        json = {
            "date_a": {"value": "2019-01-01T11:11:11Z"},
            "user": {"value": "pasio"},
            "pid": {"value": 123},
        }
        result_update = r.update(json)
        self.assertFalse(result_update[0])
        self.assertEqual(result_update[1], mock_req.put.side_effect)

    @patch("reqkintone.base.requests")
    def test_update_should_return_true_if_success_to_update(self, mock_req):
        json = {"date_a": "2019-01-01T11:11:11Z", "user": "pasio", "pid": 123}

        class response:
            status_code = 200

            def json(self):
                return json

        mock_req.put.return_value = response()

        m = Model("name")
        m.add_field("date_a", m.ATTRIBUTE_TYPE["DATETIME"])
        m.add_field("user", m.ATTRIBUTE_TYPE["STRING"])
        m.add_field("pid", m.ATTRIBUTE_TYPE["INTEGER"])
        r = Record(2, 1, m)
        r.url = "123"
        r.authorize_code = "123"
        json = {
            "date_a": {"value": "2019-01-01T11:11:11Z"},
            "user": {"value": "pasio"},
            "pid": {"value": 123},
        }
        self.assertTrue(r.update(json))
