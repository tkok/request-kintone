"""
Unit test for base.AuthorizeCode
"""

import unittest
import base64

from reqkintone.base import AuthorizeCode

class TestAuthorizeCode(unittest.TestCase):
    
    def test_initialize(self):
        a = AuthorizeCode()
        self.assertRegex(str(a), "AuthorizeCode")
        
    def test_make_code_should_raise_error_if_args_not_assigned(self):
        a = AuthorizeCode()
        with self.assertRaises(TypeError):
           a.make_code(1)
    
    def test_make_code_should_return_string(self):
        a = AuthorizeCode()
        res = a.make_code("hello", 1)
        self.assertEqual(type("string"), type(res))

    def test_make_code_should_return_expected_string(self):
        a = AuthorizeCode()
        res = a.make_code("hello", 1)
        b = base64.b64decode(res).decode("utf8")
        self.assertEqual("hello:1", b)

