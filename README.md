request-kintone
===
Useful module to communicate with kintone webapi.  
kintone is online service provided by Cybozu, Inc.  
They also provide Web API to manipulate data online.  
request-kintone make it easy to connect and handle records  
with REST API.  
Complete information link for kintone api is [kintone API](https://developer.cybozu.io/hc/ja/categories/200147600-kintone-API).
## Features
* Get define of fields of record by connecting app/form/fields.json
* Get user info by connecting users.json as user export api
* Get one record by connecting record.json
* Update target value in targeted one record
* Get records with query by connecting records.json

## How to install
```sh
pip install git+https://gitlab.com/tkok/request-kintone.git
```

## Make local config file as config_local.py
```sh
echo 'SUBDOMAIN="mysubdomain"' > request-kintone/reqkintone/config_local.py
```

## Usage
```python
import reqkintone
```  
Please check codes in examples.

## Commands for testing and examples
* Testing all
`python -m unittest discover tests`
* Testing one unittest
`python -m unittest tests.test_file_name -v`
* Execute examples
`python -m examples.get_one_record`
