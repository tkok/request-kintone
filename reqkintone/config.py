"""
Defined parametes are here like url of KintoneApi, confirmed parameters,  
endpoints of api and so on.
"""
_subdomain = "_"
try:
    from .config_local import SUBDOMAIN
    if SUBDOMAIN:
        _subdomain = SUBDOMAIN
except ImportError:
    pass

KINTONE_CONFIG = {
    "API_URL": "https://{}.cybozu.com".format(_subdomain),
    "KINTONE_API_PATH": "/k/v1",
    "USER_API_PATH": "/v1",
    "END_POINTS": {
        "RECORDS": "records.json", 
        "RECORD": "record.json",
        "USERS": "users.json",
        "APP": {
            "FORM": {
                "FIELDS": "app/form/fields.json"
                }
            }
        },
    "AUTH_HEADER": "X-Cybozu-Authorization",
    "RESPONSE_LANG": "ja",
    "REQUEST_TIMEOUT": 10,
    "REQUEST_CONTENT_TYPE_JSON": "application/json",
}
