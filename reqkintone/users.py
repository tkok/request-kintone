"""
User export api by Kintone provides registered user information by json.
Endpoint is https://(subdomain).cybozu.com/v1/users.json.
It requires authorization header.
It accepts query string, parameters are ids, codes, offset and size which has 
to be set as url query string like
`?ids[0]=1&ids[1]=2&ids[2]=3&offset=0&size=100`.
Complete guide is `https://developer.cybozu.io/hc/ja/articles/202363040#step2`.
"""
import json

from .base import RequestApi

class Users(RequestApi):
    """Class handling users api"""
    
    DEFAULT_RETURN_FIELDS = ["id", "code", "name"]

    def __init__(self):
        """
        props:
        __ids: query params for api
        __codes: query params for api
        """
        super().__init__()
        self.__codes = []
        self.__ids = []
        self.__fields = self.DEFAULT_RETURN_FIELDS
    
    def add_code(self, code):
        """
            Args:
                code (str)
        """
        if type(code) is str:
            self.__codes.append(code)
        return self.__codes

    def add_id(self, id):
        """
            Args:
                id (int)
        """
        if type(id) is int:
            self.__ids.append(id)
        return self.__ids

    def set_fields(self, fields = []):
        """Setter __fields"""
        if fields and type(fields) is list:
            self.__fields = fields

    def _query_params_as_dict(self):
        """Make query params as dict"""
        query_params = {}
        if self.__codes or self.__ids:
            def set_params(param_name):
                prop = self.__dict__["_Users__{}".format(param_name)]
                for i, v in enumerate(prop):
                    query_params["{}[{}]".format(param_name, i)] = v
                query_params["size"] = len(prop)
            
            if self.__codes:
                set_params("codes")
            elif self.__ids:
                set_params("ids")
        return query_params

    def _format_response(self, response):
        """
        Converting original json to customized data
        with reducing unnecessary fields.
        Necessary fields are set as __fields.

        Args:
            response (json): successful response from api 
        Returns:
            formatting (list): formated list includes user info with fields
        """
        formatting = []
        for user in response["users"]:
            u = {}
            for field in self.__fields:
                if field in user:
                    u[field] = user[field]
            if u:
                formatting.append(u)

        return formatting

    def get(self):
        """
        Get request to kintone with params
        Returns:
            Bool: True (status is 200), False (status not 200)
            response (object)
        """

        status, response = self._get(self._query_params_as_dict())
        if status != 200:
            return False, response
        elif type(response) is dict and "users" not in response:
            return False, response

        return True, self._format_response(response)