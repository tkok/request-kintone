import os, base64, json
from datetime import datetime
import requests
from requests.exceptions import Timeout, ConnectionError

from .config import KINTONE_CONFIG


class AuthorizeCode:
    """Class regarding authorization code
    """

    def make_code(self, login_id, login_pass):
        """make code to authorize for kintone
        Args:
            login_id (str)
            login_pass (str)
        Returns:
            code for authorization (str)
        """
        base_str = "%s:%s" % (login_id, login_pass)
        base_str = base_str.encode("utf8")
        return base64.b64encode(base_str).decode("utf8")


class RequestApi:
    """Basement class to request web api
    """

    def __init__(self):
        self.__authorize_code = None
        self.__request_error = None
        self.__url = None
        self.__request_content_type = None

    @property
    def request_error(self):
        return self.__request_error

    @property
    def authorize_code(self):
        return self.__authorize_code

    @authorize_code.setter
    def authorize_code(self, authorize_code):
        self.__authorize_code = authorize_code

    @property
    def url(self):
        return self.__url

    @url.setter
    def url(self, url):
        self.__url = url
    
    def set_request_content_type_as_json(self):
        self.__request_content_type \
            = KINTONE_CONFIG["REQUEST_CONTENT_TYPE_JSON"]

    def _set_headers(self):
        """Set request headers

        Content-type is application/json in default.
        Set authorization header if authorize code is assigned.
        """
        headers = {}
        if self.__request_content_type:
            headers = {"Content-type": self.__request_content_type}

        if self.__authorize_code:
            headers[KINTONE_CONFIG["AUTH_HEADER"]] = self.__authorize_code
        return headers

    def _get(self, params={}):
        """get request kintone api with requests

        Url is required. Headers are fixed. Response should be json.
        Args:
            params (dict) request params
        Returns:
            response.json() json object as response body
        """
        if not self.__url:
            return False, None
        try:
            content_is_json = self.__request_content_type == \
                KINTONE_CONFIG["REQUEST_CONTENT_TYPE_JSON"]
            
            args = {
                "headers": self._set_headers(),
                "timeout": KINTONE_CONFIG["REQUEST_TIMEOUT"]
            }
            if content_is_json:
                args["json"] = params
            else:
                args["params"] = params
            
            response = requests.get(self.__url, **args)

        except Timeout as err:
            self.__request_error = err
            return False, self.__request_error

        except ConnectionError as err:
            self.__request_error = err
            return False, self.__request_error
        
        try:
            return response.status_code, response.json()
        except json.decoder.JSONDecodeError as e:
            return response.status_code, e

    def _put(self, params={}):
        """put request kintone api with requests
        Url and params are required.
        Args:
            params (dict) request params
        Returns:
            response.json() json object as response body
            False in case of execution error or thrown error
        """
        if not self.__url or not params:
            return False, None

        try:
            response = requests.put(
                self.__url,
                json=params,
                headers=self._set_headers(),
                timeout=KINTONE_CONFIG["REQUEST_TIMEOUT"],
            )

        except Timeout as err:
            self.__request_error = err
            return False, self.__request_error

        except ConnectionError as err:
            self.__request_error = err
            return False, self.__request_error
        return response.status_code, response.json()


class Model:
    """Class making defined model

    Response models are defined by Kintone which are different from data python
    can execute, And basically all data in response json is string.
    To convert all data into data available on python, it requires to define 
    Model having corresponded properties.
    For example, response string defined as DATETIME by Kintone that will 
    convert into datetime object of python.
    """

    ATTRIBUTE_TYPE = {
        "INTEGER": 1, 
        "STRING": 2, 
        "DATETIME": 3,
        "LIST": 4,
        "DICTIONARY":5
    }

    def __init__(self, name):
        self.name = name
        self.fields = {}

    def add_field(self, name, type):
        """Define and Add new field
        Args:
            name (str) field name
            type (int) self.ATTRIBUTE_TYPE
        Returns:
            self.fields[name] (dict) assigned field
            False if existed fields
        """
        if name not in self.fields:
            self.fields[name] = {"type": type, "value": None}
            return self.fields[name]
        return False

    def set_value(self, name, value):
        """Set value of field
        Args:
            name (str) field name
            value (str)
        Returns:
            fields[name]["value"] (int, str, date) assigned value
        """
        if name not in self.fields:
            return False
        converted_value = self._convert(name, value)
        if not converted_value:
            return False

        self.fields[name]["value"] = converted_value
        return self.fields[name]["value"]

    def _convert(self, name, value):
        """Return converted value as defined type
        Returns:
            converted_value(int, str, date)
            False if failed
        """
        try:
            if self.fields[name]["type"] == self.ATTRIBUTE_TYPE["INTEGER"]:
                return int(value)
            elif self.fields[name]["type"] == self.ATTRIBUTE_TYPE["STRING"]:
                return str(value)
            elif self.fields[name]["type"] == self.ATTRIBUTE_TYPE["DATETIME"]:
                return datetime.strptime(value, "%Y-%m-%dT%H:%M:%SZ")
            else:
                return False
        except ValueError:
            return False
        except TypeError:
            return False

