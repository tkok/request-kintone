from .base import RequestApi, Model


class Record(RequestApi):
    """Class handling record api"""
    def __init__(self, app_id, record_id, model):
        """
        Args:
            app_id (int)
            record_id (int)
            model (Model): Defined Model Object
        """
        super().__init__()
        self.__app_id = app_id
        self.__record_id = record_id

        if type(model) != Model:
            raise TypeError("model is unexpected value")

        self.__model = model

    def _set_value(self, response):
        for name, value in response.items():
            self.__model.set_value(name, value['value'])
        return self.__model

    def get(self):
        """ Fetch a target record
        _get() returns response from KintoneApi
        and then _mapping_record is filtering the response
        Returns:
            (bool) status is 200 or not
            self.__model with assigned value
        """
        params = {"app": self.__app_id, "id": self.__record_id}
        status, response = self._get(params)
        if status != 200:
            return False, response

        assigned_to_model = self._set_value(response[self.__model.name])
        
        if not assigned_to_model:
            return False, response

        return True, self.__model

    def update(self, new_params={}):
        """Update target values in record
        Args:
            new_params (dict)
        Return:
            boolean
        """
        if not new_params:
            return False, None

        params = {
            "app": self.__app_id, 
            "id": self.__record_id, 
            "record": new_params
            }
        status, response = self._put(params)
        if status != 200:
            return False, response

        return True, response
