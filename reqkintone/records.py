import json
import copy
from .base import RequestApi, Model


class Records(RequestApi):
    """Class handling records api"""

    def __init__(self, app_id, model):
        """
        Args:
            app_id (int)
            model (Model): Defined Model Object
        """
        super().__init__()
        self.__app_id = app_id

        if type(model) != Model:
            raise TypeError("model is unexpected value")

        self.__model = model
        self.__total_count = True
        self.__query = None
        self.__models = []

    def set_query(self, query):
        if type(query) != Query:
            raise TypeError("query is unexpected value")
        self.__query = query

    def set_total_count(self, total_count_flg):
        if type(total_count_flg) == bool:
            self.__total_count = total_count_flg

    def _set_values(self, response):
        """
        Assigned value of Model based on response.
        Args:
            response (list): json from response.body as Kintone.records
        """
        for record in response:
            m = copy.deepcopy(self.__model)
            for name, value in record.items():
                m.set_value(name, value["value"])

            self.__models.append(m)
        return self.__models

    def get(self):
        """
        Get request to kintone with params
        Acceptable params
            fields, app(required), query, totalCount
        Returns:
            Bool: True (status is 200), False (status not 200)
            
            response (object): In case request is error
            or
            self.__models (list): List of Model assigned value based on response
        """

        params = {
            "app": self.__app_id,
            "fields": list(self.__model.fields),
            "totalCount": "true" if self.__total_count else "false",
        }
        if self.__query and self.__query.query_string():
            params["query"] = self.__query.query_string()

        status, response = self._get(params)
        if status != 200:
            return False, response

        assigned_to_model = self._set_values(response[self.__model.name])

        if not assigned_to_model:
            return False, response

        return True, self.__models


class Query:
    def __init__(self):
        self.__queries = []
        self.__orders = []
        self.__limit = -1
        self.__offset = -1

    @property
    def queries(self):
        return self.__queries

    def set_limitation(self, offset, limit=0):
        """Assign offset and limit
        Args:
            offset (int)
            limit (int) default=0
        """
        if type(offset) == int and offset > 0:
            self.__offset = offset

        if type(limit) == int and limit > 0:
            self.__limit = limit

    def set_order(self, key, asc=True):
        """Set order options
        Args:
            key (str): target key for order
            asc (bool): True is asc, False is desc
        """
        self.__orders.append({"key": key, "asc": asc})

    def make_condition(self, key, val, operator):
        """Make one query and return as string
        Args:
            key (str): target column for query
            val (int|str): value for query
                if value is str it is added escape string as backslash
            operator (str): Pattern defined by Kintone
        Returns:
            cond (str): One query string formatted following Kintone rule
        """
        cond = "{} {} ".format(key, operator)
        if type(val) == str:
            cond += '"{}"'.format(val)
        else:
            cond += "{}".format(val)

        return cond

    def add(self, key, val, operator, and_logical=True):
        condition_by_str = self.make_condition(key, val, operator)
        return self.add_by_str(condition_by_str, False, and_logical=and_logical)

    def add_by_str(self, string, group=True, and_logical=True):
        q = {}
        q["query_string"] = "({})".format(string) if group else "{}".format(string)
        q["and_logical"] = and_logical
        self.__queries.append(q)
        return self.__queries

    def query_string(self, option=True):
        """
        Return query string based on assigned variables
        Args:
            option (bool): Return string involve option queries or not.
        Returns:
            query_string (string)
        """
        query_string = ""
        for i, query in enumerate(self.__queries):
            if i > 0:
                # and, or
                if query["and_logical"]:
                    query_string += " and "
                else:
                    query_string += " or "

            query_string += query["query_string"]

        if query_string:
            query_string += " "

        query_string += self._make_option_query()

        return query_string

    def _make_option_query(self):
        """
        Make option query by string
        it contains order, limit, offset
        Returns:
            option (string)
        """
        option = ""
        for i, order in enumerate(self.__orders):
            if i == 0:
                option += "order by "
            if i >= 1:
                option += ", "
            asc = "asc" if order["asc"] else "desc"
            option += "{} {}".format(order["key"], asc)

        if self.__limit >= 0:
            if option:
                option += " "
            option += "limit {}".format(self.__limit)

        if self.__offset >= 0:
            if option:
                option += " "
            option += "offset {}".format(self.__offset)

        return option
