"""
fields module

"""
import json

from .base import RequestApi


class Fields(RequestApi):
    """Class handling app/form/fields api"""

    def __init__(self, app_id):
        """
        props:
        __names: list of field name to return
        """
        super().__init__()
        self.__app_id = app_id
        self.__names = []

    def add_name(self, name):
        """
            Args:
                name (str)
        """
        if type(name) is str:
            self.__names.append(name)
        return self.__names

    def _format_response(self, items):
        """
        Converting original json to customized data
        with reducing unnecessary fields.
        Necessary fields are set as __names.
        __names is [] means to return all fields.

        Args:
            response (json): successful response from api 
        Returns:
            formatting (list): formated list
        """
        if not self.__names:
            return items

        formatting = {}
        for item in items:
            for name in self.__names:
                if name == item:
                    formatting[name] = items[name]

        return formatting

    def get(self):
        status, response = self._get({"app": self.__app_id})
        if status != 200:
            return False, response
        elif type(response) is dict and "properties" not in response:
            return False, response

        return True, self._format_response(response["properties"])
