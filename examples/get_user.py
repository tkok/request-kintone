"""
Example to get users
"""
from examples.settings import LOGIN_ID, LOGIN_PASS, USER_CODES
from reqkintone.base import AuthorizeCode
from reqkintone.users import Users
import reqkintone.config as config


def main():
    """Fetch records from api with configure in setting"""
    u_api = Users()
    for u in USER_CODES:
        u_api.add_code(u)

    u_api.authorize_code = AuthorizeCode().make_code(LOGIN_ID, LOGIN_PASS)
    u_api.url = (
        config.KINTONE_CONFIG["API_URL"]
        + config.KINTONE_CONFIG["USER_API_PATH"]
        + "/"
        + config.KINTONE_CONFIG["END_POINTS"]["USERS"]
    )
    record = u_api.get()
    print(record)

if __name__ == "__main__":
    main()
