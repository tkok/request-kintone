"""
Example to get one record by record_id
"""
from examples.settings import APP_ID, LOGIN_ID, LOGIN_PASS, RECORD_ID, FIELDS_DEFINE
from reqkintone.base import AuthorizeCode, Model
from reqkintone.record import Record
import reqkintone.config as config


def main():
    """Fetch one record from api with configure in setting"""
    model = Model("record")
    for f in FIELDS_DEFINE:
        model.add_field(f["field_name"], model.ATTRIBUTE_TYPE[f["field_type"]])
    r_api = Record(APP_ID, RECORD_ID, model)
    r_api.authorize_code = AuthorizeCode().make_code(LOGIN_ID, LOGIN_PASS)
    r_api.url = (
        config.KINTONE_CONFIG["API_URL"]
        + config.KINTONE_CONFIG["KINTONE_API_PATH"]
        + "/"
        + config.KINTONE_CONFIG["END_POINTS"]["RECORD"]
    )
    r_api.set_request_content_type_as_json()
    record = r_api.get()
    print(record[0], model.fields)


if __name__ == "__main__":
    main()
