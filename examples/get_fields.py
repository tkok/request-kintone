"""
Example to get fields info
"""
from examples.settings import APP_ID, LOGIN_ID, LOGIN_PASS, RECORD_ID, FIELDS_DEFINE
from reqkintone.base import AuthorizeCode, Model
from reqkintone.fields import Fields
import reqkintone.config as config


def main():
    """Fetch fields info from api with configure in setting"""
    f = Fields(APP_ID)
    f.authorize_code = AuthorizeCode().make_code(LOGIN_ID, LOGIN_PASS)
    f.url = (
        config.KINTONE_CONFIG["API_URL"]
        + config.KINTONE_CONFIG["KINTONE_API_PATH"]
        + "/"
        + config.KINTONE_CONFIG["END_POINTS"]["APP"]["FORM"]["FIELDS"]
    )
    f.add_name(FIELDS_DEFINE[0]["field_name"])
    f.add_name(FIELDS_DEFINE[1]["field_name"])
    res = f.get()
    print(res)


if __name__ == "__main__":
    main()
