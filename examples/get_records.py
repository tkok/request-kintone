"""
Example to get records
"""
from examples.settings import APP_ID, LOGIN_ID, LOGIN_PASS, RECORD_ID, FIELDS_DEFINE
from reqkintone.base import AuthorizeCode, Model
from reqkintone.records import Records, Query
import reqkintone.config as config


def main():
    """Fetch records from api with configure in setting"""
    model = Model("records")
    for f in FIELDS_DEFINE:
        model.add_field(f["field_name"], model.ATTRIBUTE_TYPE[f["field_type"]])
    r_api = Records(APP_ID, model)
    r_api.authorize_code = AuthorizeCode().make_code(LOGIN_ID, LOGIN_PASS)
    r_api.url = (
        config.KINTONE_CONFIG["API_URL"]
        + config.KINTONE_CONFIG["KINTONE_API_PATH"]
        + "/"
        + config.KINTONE_CONFIG["END_POINTS"]["RECORDS"]
    )
    query = Query()
    query.add(FIELDS_DEFINE[0]["field_name"], 1, ">")
    query.add(FIELDS_DEFINE[1]["field_name"], "hello", "!=")
    query.set_limitation(1, 5)
    query.set_order(FIELDS_DEFINE[0]["field_name"], False)
    r_api.set_query(query)
    record = r_api.get()

    print(query.query_string())
    print([r.fields for r in record[1]])


if __name__ == "__main__":
    main()
