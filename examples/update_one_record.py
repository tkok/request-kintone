"""
Example to update one record by record_id
"""
from examples.settings import (
    APP_ID, 
    LOGIN_ID, 
    LOGIN_PASS,
    RECORD_ID,
    UPDATE_RECORD
)
from reqkintone.base import AuthorizeCode, Model
from reqkintone.record import Record
import reqkintone.config as config
def main():
    '''Fetch one record from api with configure in setting'''
    model = Model("record")
    model.add_field("レコード番号", model.ATTRIBUTE_TYPE["INTEGER"])
    r_api = Record(APP_ID, RECORD_ID, model)
    r_api.authorize_code = AuthorizeCode().make_code(LOGIN_ID, LOGIN_PASS)
    r_api.url = config.KINTONE_CONFIG["API_URL"] \
        + config.KINTONE_CONFIG["KINTONE_API_PATH"] + "/" \
        + config.KINTONE_CONFIG["END_POINTS"]["RECORD"]
    
    result = r_api.update(UPDATE_RECORD)
    print(result)

if __name__ == '__main__':
    main()
